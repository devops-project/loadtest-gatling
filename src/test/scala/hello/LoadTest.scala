package hello

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder


/**
 * mvn gatling:test
 */
class LoadTest extends Simulation {

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl("http://recette.myrhis.fr/employee-service/employee/65664/all?page=0&size=10&filterStatut=true&filterStatut1=false&filterName=")

  object HelloWorldResource {
    val get: ChainBuilder = exec(http("HelloWorld")
      .get("/"))
      //.basicAuth("user", "24gh39ugh0"))
  }

  val myScenario: ScenarioBuilder = scenario("RampUpUsers")
    .exec(HelloWorldResource.get)

  setUp(myScenario.inject(
    incrementUsersPerSec(50)
      .times(5)
      .eachLevelLasting(5 seconds)
      .separatedByRampsLasting(5 seconds)
      .startingFrom(20)
  )).protocols(httpProtocol)
    .assertions(global.successfulRequests.percent.is(100))
}